# Hackaton
Projet spring qui marche bien. Lisez bien les choses à installer avant d'executer le lacement

## Pré-requis
- Extraire apache maven dans votre ordinateur (utils)
- Ajouter le path de C:\Program Files\apache-maven-3.6.3\bin dans votre variable d'environnement

## Lancement
- mvn clean install
- ./mvnw spring-boot:run

## Documentation 
- [Site officiele spring Boot](https://spring.io/quickstart)
- [Regle de l'architecture Rest](https://blog.nicolashachet.com/developpement-php/larchitecture-rest-expliquee-en-5-regles/)
- [ResponseEntity](https://www.baeldung.com/spring-response-entity)
- [Jackson](https://www.baeldung.com/jackson)

## Remarque
- La commande de lancement doit être exécutée dans le même dossier que pom.xml
- Changer la version de java de votre pom.xml
- N'enlève pas le fichier .gitIgnore
