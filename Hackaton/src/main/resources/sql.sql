create table client (
    id serial primary key,
    email varchar(50),
    mdp varchar(50),
    nom varchar(50),
    prenom varchar(50),
    etat int,
    taille double precision
);

create table critere (
    id serial primary key,
    value1 varchar(255),
    value2 varchar(255),
    data varchar(255)
    type int
);

create table config (
    id serial primary key,
    value1 varchar(255),
    value2 varchar(255),
    data varchar(255),
    type int,
    loginid int
);
alter table config add foreign key (loginid) references client(id);


insert into client(email,mdp,nom,prenom,etat,taille) values ('hasina@gmail.com','motdepasse','Hasina','Rivonandrasana',0,1.75);
insert into client(email,mdp,nom,prenom,etat,taille) values ('hery@gmail.com','motdepasse','Hery','Rivonandrasana',0,1.53);
insert into client(email,mdp,nom,prenom,etat,taille) values ('heritsaina@gmail.com','motdepasse','Heritsaina','Rivonandrasana',0,1.61);
insert into client(email,mdp,nom,prenom,etat,taille) values ('hana@gmail.com','motdepasse','Hana','Rivonandrasana',0,1.82);

insert into critere(value1,value2,data,type) values(null,1.40,0.5,50);
insert into critere(value1,value2,data,type) values(1.41,1.50,0.6,50);
insert into critere(value1,value2,data,type) values(1.51,1.60,0.7,50);
insert into critere(value1,value2,data,type) values(1.61,1.70,0.8,50);
insert into critere(value1,value2,data,type) values(1.71,1.80,0.9,50);
insert into critere(value1,value2,data,type) values(1.81,null,1.0,50);

insert into critere(value1,value2,data,type) values(null,1.40,0.6,100);
insert into critere(value1,value2,data,type) values(1.41,1.50,0.7,100);
insert into critere(value1,value2,data,type) values(1.51,1.60,0.8,100);
insert into critere(value1,value2,data,type) values(1.61,1.70,0.9,100);
insert into critere(value1,value2,data,type) values(1.71,1.80,1.0,100);
insert into critere(value1,value2,data,type) values(1.81,null,1.1,100);

insert into critere(value1,value2,data,type) values(null,'06:00:00',19,150);
insert into critere(value1,value2,data,type) values('06:01:00','08:00:00',21,150);
insert into critere(value1,value2,data,type) values('08:01:00','10:00:00',23,150);
insert into critere(value1,value2,data,type) values('10:01:00','12:00:00',25,150);
insert into critere(value1,value2,data,type) values('12:01:00','14:00:00',27,150);
insert into critere(value1,value2,data,type) values('14:01:00','16:00:00',29,150);
insert into critere(value1,value2,data,type) values('16:01:00','18:00:00',31,150);
insert into critere(value1,value2,data,type) values('18:01:00',null,32,150);

insert into config(value1,value2,data,type,loginid) values(null,null,0.75,50,1);
insert into config(value1,value2,data,type,loginid) values(null,null,0.4,100,2);
insert into config(value1,value2,data,type,loginid) values('06:00:00','12:00:00',17,150,3);

