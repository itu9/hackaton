package extract.chrono;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.epreuve.sous.Tache;
import extract.model.HCrud;

@Service
public class ChronoSer extends HCrud<Chrono, Integer, ChronoRepos> {

    @Autowired
    @Override
    public void setRepos(ChronoRepos repos) {
        super.setRepos(repos);
    }

    public List<Chrono> findByTache(Tache tache) {
        List<Chrono> ans = this.getRepos().findByTache(tache);
        Collections.sort(ans);
        for (int i = 0; i < ans.size(); i++) {
            ans.get(i).setTache(null);
        }
        return ans;
    }

}
