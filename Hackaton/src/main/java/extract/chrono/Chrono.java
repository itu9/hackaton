package extract.chrono;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import extract.epreuve.sous.Tache;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Chrono extends HElement<Integer> implements Comparable<Chrono>{
    Timestamp date;
    Integer state;
    /**
     * 0 -> Demarer
     * 50 -> Pause
     * 100 -> Reprendre
     * 150 -> Terminer
     */

    @OneToOne
    @JoinColumn(name = "soustacheid")
    Tache tache;

    public Chrono() {
        this.setDate(Timestamp.valueOf(LocalDateTime.now()));
    }

    public static int[] eval(List<Chrono> chronos) {
        int[] ans = new int[3];
        boolean pause = false;
        for (int i = 0; i < chronos.size(); i++) {
            if (chronos.get(i).getState().equals(0) || chronos.get(i).getState().equals(150)) {
                pause = false;
            }
            if (chronos.get(i).getState().equals(0)) {
                ans[0] = 0; // h
                ans[1] = 0; // m
                ans[2] = 0; // s
                continue;
            }
            if (chronos.get(i).getState().equals(100)) {
                pause = false;
                continue;
            }
            if (pause && !chronos.get(i).getState().equals(150)) {
                continue;
            }
            if (chronos.get(i).getState().equals(50)) {
                pause = true;
            }
            long time = (chronos.get(i).getDate().getTime() - chronos.get(i - 1).getDate().getTime()) / 1000;
            int[] timeTemp = Chrono.eval(time);
            ans = Chrono.add(ans, timeTemp);
        }
        return ans;
    }

    public static int[] add(int[] first, int[] second) {
        Chrono.addSec(first, second[2]);
        Chrono.addMin(first, second[1]);
        Chrono.addHour(first, second[0]);
        return first;
    }

    public static void addSec(int[] first, int s) {
        int sec = first[2] + s;
        first[2] = (sec % 60);
        Chrono.addMin(first, (int) (sec / 60));
    }

    public static void addMin(int[] first, int m) {
        int min = first[1] + m;
        first[1] = (min % 60);
        Chrono.addHour(first, (int) (min / 60));
    }

    public static void addHour(int[] first, int h) {
        first[0] += h;
    }

    public static int[] eval(long time) {
        int[] ans = new int[3];
        ans[0] = (int) (time / 3600);
        time -= (ans[0] * 3600);
        ans[1] = (int) (time / 60);
        time -= (ans[1] * 60);
        ans[2] = (int) time;
        return ans;
    }

    @Override
    public int compareTo(Chrono arg0) {
        return this.getDate().compareTo(arg0.getDate());
    }

}
