package extract.chrono;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import extract.epreuve.sous.Tache;

public interface ChronoRepos extends JpaRepository<Chrono, Integer> {
    public List<Chrono> findByTache(Tache tache);

}
