package extract.chrono;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@RestController()
@RequestMapping("/chrono")
@CrossOrigin("*")
public class ChronoCnt extends CrudController<Chrono, Integer, ChronoRepos, ChronoSer> {

    @Autowired
    public ChronoCnt(ChronoSer service) {
        super(service);
    }

}
