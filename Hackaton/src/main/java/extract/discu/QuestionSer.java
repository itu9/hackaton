package extract.discu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.model.HCrud;

@Service
public class QuestionSer extends HCrud<Question, Integer,QuestionRepos> {
    @Autowired
    ResponseSer sResponse;

    @Autowired
    @Override
    public void setRepos(QuestionRepos repos) {
        super.setRepos(repos);
    }

    @Override
    public Question findById(Integer obj) {
        Question quest = super.findById(obj);
        quest.setResponse(this.sResponse.getByQuestion(quest));
        return quest;
    }

}
