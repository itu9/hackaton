package extract.discu;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import extract.auth.Login;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Question extends HElement<Integer> {
    @OneToOne
    @JoinColumn(name = "etudiantid")
    Login login;

    String value;

    String desc;

    String img;

    @Transient
    List<Response> response;
}
