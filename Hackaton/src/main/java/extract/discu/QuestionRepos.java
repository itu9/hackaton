package extract.discu;

import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepos extends JpaRepository<Question,Integer> {
    
}
