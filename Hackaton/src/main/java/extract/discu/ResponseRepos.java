package extract.discu;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponseRepos extends JpaRepository<Response,Integer> {
    public List<Response> findByQuestion(Question question);
}
