package extract.discu;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import extract.auth.Login;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Response extends HElement<Integer> {
    @OneToOne
    @JoinColumn(name = "questionid")
    Question question;

    String value;

    String desc;

    String img;

    @OneToOne
    @JoinColumn(name = "loginid")
    Login etudiant;
}
