package extract.discu;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.model.HCrud;

@Service
public class ResponseSer extends HCrud<Response,Integer,ResponseRepos>{
    @Autowired
    @Override
    public void setRepos(ResponseRepos repos) {
        // TODO Auto-generated method stub
        super.setRepos(repos);
    }

    public List<Response> getByQuestion(Question question ) {
        List<Response> resp = this.getRepos().findByQuestion(question);
        for (int i = 0; i < resp.size(); i++) {
            resp.get(i).setQuestion(null);
        }
        return resp;
    }
}
