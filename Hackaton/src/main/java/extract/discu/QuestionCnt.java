package extract.discu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@RestController()
@RequestMapping("/question")
@CrossOrigin("*")
public class QuestionCnt extends CrudController<Question, Integer, QuestionRepos,QuestionSer> {

    @Autowired
    public QuestionCnt(QuestionSer service) {
        super(service);
    }

}
