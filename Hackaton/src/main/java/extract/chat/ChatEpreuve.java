package extract.chat;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "epreuve_chatreponse")
public class ChatEpreuve extends HElement<Integer> {
    Integer epreuveid;
    @OneToOne
    @JoinColumn(name = "chatresponseid")
    Chat chat;   
}
