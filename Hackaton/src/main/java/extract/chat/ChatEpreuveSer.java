package extract.chat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.epreuve.Epreuve;
import extract.model.HCrud;

@Service
public class ChatEpreuveSer extends HCrud<ChatEpreuve, Integer, ChatEpreuveRepos> {

    @Autowired
    ChatRepository rChat;

    @Autowired
    @Override
    public void setRepos(ChatEpreuveRepos repos) {
        super.setRepos(repos);
    }
    

    @Override
    public ChatEpreuve create(ChatEpreuve obj) {
        this.rChat.save(obj.getChat());
        return super.create(obj);
    }


    public List<ChatEpreuve> getByEpreuve(Epreuve epreuve) {
        List<ChatEpreuve> ans = this.getRepos().findByEpreuveid(epreuve.getId());
        return ans;
    }
}
