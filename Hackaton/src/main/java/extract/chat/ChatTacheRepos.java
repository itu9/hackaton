package extract.chat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatTacheRepos  extends JpaRepository<ChatTache,Integer>{
    public List<ChatTache> findByTacheid(Integer id);
}
