package extract.chat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.epreuve.sous.Tache;
import extract.model.HCrud;

@Service
public class ChatTacheSer extends HCrud<ChatTache, Integer, ChatTacheRepos> {

    @Autowired
    ChatRepository rChat;

    @Autowired
    @Override
    public void setRepos(ChatTacheRepos repos) {
        super.setRepos(repos);
    }

    @Override
    public ChatTache create(ChatTache obj) {
        this.rChat.save(obj.getChat());
        return super.create(obj);
    }

    public List<ChatTache> getByTaches(Tache tache) {
        return this.getRepos().findByTacheid(tache.getId());
    }

}
