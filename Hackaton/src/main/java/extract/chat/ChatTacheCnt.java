package extract.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;
import extract.epreuve.sous.Tache;

@RestController()
@RequestMapping("/chat-tache")
@CrossOrigin("*")
public class ChatTacheCnt extends CrudController<ChatTache, Integer, ChatTacheRepos,ChatTacheSer> {

    @Autowired
    public ChatTacheCnt(ChatTacheSer service) {
        super(service);
    }

    @GetMapping("/tache/{id}")
    public ResponseEntity<?> getByEpreuve(@PathVariable("id") Integer id) {
        try {
            Tache epreuve = new Tache();
            epreuve.setId(id);
            return this.returnSuccess(this.getService().getByTaches(epreuve), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }
}
