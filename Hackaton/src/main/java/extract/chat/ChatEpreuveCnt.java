package extract.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;
import extract.epreuve.Epreuve;

@RestController()
@RequestMapping("/chat-epreuve")
@CrossOrigin("*")
public class ChatEpreuveCnt extends CrudController<ChatEpreuve, Integer, ChatEpreuveRepos, ChatEpreuveSer> {

    @Autowired
    public ChatEpreuveCnt(ChatEpreuveSer service) {
        super(service);
        // TODO Auto-generated constructor stub
    }

    

    @Override
    public ResponseEntity<?> create(@RequestBody ChatEpreuve obj,@RequestParam(name = "idUser", required = false) Integer id) throws Exception {
        super.create(obj, id);
        return this.findAll(id);
    }



    @GetMapping("/epreuve/{id}")
    public ResponseEntity<?> getByEpreuve(@PathVariable("id") Integer id) {
        try {
            Epreuve epreuve = new Epreuve();
            epreuve.setId(id);
            return this.returnSuccess(this.getService().getByEpreuve(epreuve), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

}
