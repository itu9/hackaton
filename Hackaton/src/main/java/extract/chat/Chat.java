package extract.chat;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import extract.auth.Login;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "chatresponse")
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Chat extends HElement<Integer> {
    String question;
    String response;
    Timestamp date;
    @OneToOne()
    @JoinColumn(name = "etudiantid")
    Login etudiant;
    public Chat() {
        this.setDate(Timestamp.valueOf(LocalDateTime.now()));
    }
    
}
