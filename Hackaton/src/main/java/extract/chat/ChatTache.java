package extract.chat;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "tache_chatreponse")
public class ChatTache extends HElement<Integer> {
    Integer tacheid;
    
    @OneToOne
    @JoinColumn(name = "chatresponseid")
    Chat chat;
}
