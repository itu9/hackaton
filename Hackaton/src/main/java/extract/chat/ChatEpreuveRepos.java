package extract.chat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatEpreuveRepos extends JpaRepository<ChatEpreuve,Integer> {
    public List<ChatEpreuve> findByEpreuveid(Integer id);
}
