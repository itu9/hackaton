package extract.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import lombok.Data;

@Data
public class Cnt {


    public ResponseEntity<SuccessResponse> returnSuccess(Object obj, HttpStatus status) {
        return new ResponseEntity<>(new SuccessResponse(obj), status);
    }

    public ResponseEntity<ErrorDisplay> returnError(Exception obj, HttpStatus status) {
        return new ResponseEntity<>(new ErrorDisplay(status, obj), status);
    }


}
