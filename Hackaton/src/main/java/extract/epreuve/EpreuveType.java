package extract.epreuve;

import javax.persistence.Entity;
import javax.persistence.Table;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "epreuvetype")
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class EpreuveType extends HElement<Integer> {
    String titre;
}
