package extract.epreuve;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EpreuveRepository extends JpaRepository<Epreuve,Integer> {
    
}
