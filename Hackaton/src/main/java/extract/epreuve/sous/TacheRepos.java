package extract.epreuve.sous;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import extract.epreuve.Epreuve;

public interface TacheRepos extends JpaRepository<Tache,Integer> {
    public List<Tache> findByEpreuve(Epreuve epreuve);
}
