package extract.epreuve.sous;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import extract.auth.Login;
import extract.chrono.Chrono;
import extract.epreuve.Epreuve;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "soustache")
@Data
@EqualsAndHashCode(callSuper = false)
public class Tache extends HElement<Integer> {
    String tache;
    Integer effectif;
    Integer niveau = 1;
    Integer h = 0;
    Integer m = 0;
    Integer s = 0;
    Integer perfection = 0;
    Integer generalise = 0;

    @OneToOne
    @JoinColumn(name = "tachetypeid", nullable = false)
    TacheType type;

    @OneToOne
    @JoinColumn(name = "etudiantid", nullable = false)
    Login etudiant;

    @OneToOne
    @JoinColumn(name = "epreuveid", nullable = false)
    Epreuve epreuve;

    @Transient
    List<Chrono> chrono;

    public int[] getEval() {
        if (this.getChrono() == null) 
            return new int[3];
        int[] ans = Chrono.eval(this.getChrono());
        this.h = ans[0];
        this.m = ans[1];
        this.s = ans[2];
        return ans;
    }

    public int getSecond() {
        int res = this.getH() * 60 * 60;
        res += this.getM() * 60;
        res += this.getS();
        return res;
    }
}
