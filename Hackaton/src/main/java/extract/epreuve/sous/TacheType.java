package extract.epreuve.sous;

import javax.persistence.Entity;
import javax.persistence.Table;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "tachetype")
@Data
@EqualsAndHashCode(callSuper = false)
public class TacheType extends HElement<Integer> {
    String titre;
}
