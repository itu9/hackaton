package extract.epreuve.sous;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.chrono.ChronoSer;
import extract.epreuve.Epreuve;
import extract.model.HCrud;

@Service
public class TacheService extends HCrud<Tache, Integer, TacheRepos> {
    @Autowired
    ChronoSer sChrono;

    @Autowired
    @Override
    public void setRepos(TacheRepos repos) {
        super.setRepos(repos);
    }

    @Override
    public Tache findById(Integer obj) {
        Tache tache = super.findById(obj);
        tache.setChrono(this.sChrono.findByTache(tache));
        return tache;
    }

    public List<Tache> getByEpreuve(Epreuve epreuve) {
        List<Tache> response = this.getRepos().findByEpreuve(epreuve);
        for (int i = 0; i < response.size(); i++) {
            response.get(i).setEpreuve(null);
            response.get(i).setChrono(this.sChrono.findByTache(response.get(i)));
        }
        return response;
    }

}
