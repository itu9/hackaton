package extract.epreuve.sous;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;
import lombok.Data;
import lombok.EqualsAndHashCode;

@RestController()
@RequestMapping("/etaches")
@CrossOrigin("*")
@Data
@EqualsAndHashCode(callSuper = false)
public class TacheCnt extends CrudController<Tache,Integer,TacheRepos,TacheService> {

    @Autowired
    public TacheCnt(TacheService service) {
        super(service);
        //TODO Auto-generated constructor stub
    }
    
}
