package extract.epreuve;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.epreuve.sous.TacheService;
import extract.model.HCrud;

@Service
public class EpreuveService extends HCrud<Epreuve, Integer, EpreuveRepository> {

    @Autowired
    TacheService sTache;

    @Autowired
    @Override
    public void setRepos(EpreuveRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public Epreuve findById(Integer obj) {
        Epreuve epreuve = super.findById(obj);
        epreuve.setTaches(this.sTache.getByEpreuve(epreuve));
        epreuve.getT();
        epreuve.eval();
        return epreuve;
    }

}
