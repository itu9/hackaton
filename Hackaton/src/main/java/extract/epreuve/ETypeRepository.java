package extract.epreuve;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ETypeRepository extends JpaRepository<EpreuveType, Integer> {
    public List<EpreuveType> findByTitre(String titre);
}
