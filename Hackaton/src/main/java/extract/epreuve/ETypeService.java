package extract.epreuve;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.model.HCrud;

@Service
public class ETypeService extends HCrud<EpreuveType,Integer,ETypeRepository> {

    @Autowired
    @Override
    public void setRepos(ETypeRepository repos) {
        super.setRepos(repos);
    }
}
