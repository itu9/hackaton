package extract.epreuve;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@RestController()
@RequestMapping("/etypes")
@CrossOrigin("*")
public class ETypeCnt extends CrudController<EpreuveType,Integer,ETypeRepository,ETypeService> {

    @Autowired
    public ETypeCnt(ETypeService service) {
        super(service);
        //TODO Auto-generated constructor stub
    }
    
}
