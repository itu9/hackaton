package extract.epreuve;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@RestController()
@RequestMapping("/epreuves")
@CrossOrigin("*")
public class EpreuveCnt extends CrudController<Epreuve, Integer, EpreuveRepository, EpreuveService> {

    @Autowired
    public EpreuveCnt(EpreuveService service) {
        super(service);
    }

    

}
