package extract.epreuve;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import extract.auth.Login;
import extract.epreuve.sous.Tache;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "epreuve")
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Epreuve extends HElement<Integer> {
    Integer h = 0;
    Integer m = 0;
    Integer s = 0;
    String titre;

    @Transient
    boolean depass;

    @OneToOne
    @JoinColumn(name = "etudiantid", nullable = false)
    Login etudiant;

    @OneToOne
    @JoinColumn(name = "epreuvetypeid", nullable = false)
    EpreuveType type;

    @Transient
    List<Tache> taches = new ArrayList<Tache>();

    @Transient
    Vitesse vitesse;

    public int getT() {
        this.setVitesse(new Vitesse(this.getTotal()));
        return  0;
    }

    public long getTotal() {
        long res = 0;
        for (int i = 0; i < this.getTaches().size(); i++) {
            this.getTaches().get(i).getEval();
            res += this.getTaches().get(i).getSecond();
        }
        return res;
    }

    public long getMyTotal() {
        long res = this.getS();
        res += (this.getM() * 60);
        res += (this.getH() * 3600);
        return res;
    }

    public void eval() {
        this.setDepass(this.getTotal() > this.getMyTotal());
    }

}
