package extract.epreuve;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Vitesse {
    int h;
    int m;
    int s;
    public Vitesse(long res) {
        int h = (int) (res / 3600);
        res -= (h * 3600);
        int m = (int) (res / 60);
        res -= (m * 60);
        int s = (int) res;
        this.setH(h);
        this.setM(m);
        this.setS(s);
        
    }

    
}
